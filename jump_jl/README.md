# Использование языка Julia для решения задач оптимизации.

## Установка
1. Идёте на https://julialang.org/downloads/
2. Выбираете подходящий пакет, где `1.X.X` -- номер последней версии (на даннный момент - `1.9.4`):
    1. Для Windows можно скачать установщик, тогда в меню появится ярлык запуска консоли Julia
    2. Или же можно скачать portable версию, тогда нужно будет запустить
    >`julia-1.X.X-win64\julia-1.X.X\bin\julia.exe`
    3. Для Mac можно так же скачать установщик или же архив, где нужно зпустить
    >`julia-1.X.X-macaarch64/julia-1.X.X/bin/julia`
    4. Для Linux скачиваете архив и запускаете:
    >`julia-1.X.X-linux-x86_64/julia-1.X.X/bin/julia`
3. Так или иначе должно запуститься окно консоли с логотипом языка Julia
4. В консоли нажимаем клавишу `]` -- режим пакетного менеджера.
5. Добавляем пакет с интерактивными блокнотами `Pluto`:
    > `(@v1.9) pkg> add Pluto`
6. После окончания загрузки клавишей `Backspace` возвращаемся в обычный режим консоли и запускаем `Pluto`:
    > `julia> using Pluto; Pluto.run()`
7. Когда закончится загрузка, в браузере должна открыться страничка блокнота, где можно создать собственный новый блокнот или открыть `jump.jl`, чтобы запустить гайд по оптимизатору `JuMP`.