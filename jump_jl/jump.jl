### A Pluto.jl notebook ###
# v0.19.27

using Markdown
using InteractiveUtils

# ╔═╡ 03d91ab4-63ab-11ee-0d25-21124b615428
begin
	using JuMP, HiGHS
end

# ╔═╡ 6825af41-9c57-49fb-b3f3-163340bb056b
md"""
# Привет, приятель!
"""

# ╔═╡ 5e77a508-92d5-42a9-a38b-c1ed6f6ea820
md"""
В Pluto, в отличие от других блокнотных сред разработки, все клетки с кодом выполняются сразу. Блокнот работет *реактивно*. Дождись подгрузки и компиляции пакетов (это может занять несколько минут) и можешь в интерактивном режиме менять значения переменных. Код будет выполняться немедленно. Если возникает ошибка, то она не прячется под корягой, а сразу крашит тебе весь блокнот: не пропустишь.
"""

# ╔═╡ 266919f1-a494-4372-92cd-36d1b41be0c9
md"""
Ключевые команды найдёшь нажав `F1`
"""

# ╔═╡ c7189e1f-5bb9-4a21-83f9-fcad9c61336b
md"""
В клетке может быть либо:
* одна команда;
* блок `begin`...`end`, обычный код, как ты привык;
* изолированный блок `let`...`end`, который не модифицирует переменные снаружи

Поскольку блокнот реактивный, то каждая переменная может быть объявлена только в одном месте, иначе блокнот ругается.
"""

# ╔═╡ 5c48657b-ba81-42f1-a342-84e6afa65844
# Одна команда:
cos(22/7)

# ╔═╡ 32040a18-50f2-4c36-a415-7c49a06ee507
# Блок с несколькими командами:
let
	a = [] # т.к. мы внутри блока let, переменная a не будет видна снаружи

	for i in 1:5
		# функции, которые модифицируют свои аргументы, принято называть !
		push!(a, i)
	end
	
	# Вывод послеждней команды появится над клеткой 
	a[1], a, a[end] # Да, нумерация массивов начинается с 1
end

# ╔═╡ eccd5bb4-93fc-499a-b490-e5c23464c411
md"""
Помнишь, мы устанавливали `Pluto` в консоли? Мы установили его в базовое пакетное окружение в твоём компе. Внутри блокнота же есть своё обособленно пакетное окружение, это нужно для того, чтобы когда ты захочешь поделиться блокнотом, он везде запускался одинаково. Вот и сейчас, пока ты это читаешь, блокнот устанавливает указанные в клетке ниже пакеты, чтобы ты увидел то же самое, что вижу сейчас я, пока составляю этот текст. 
"""

# ╔═╡ 1cdeca84-fec7-4b86-8be0-3aa742dc81e6
md"""
Содержимое клетки можно скрыть, нажав кнопку с глазом слева. По умолчанию результат выполнения команды выводится над клеткой. Если поставишь `;` -- вывод будет скрыт. 
"""

# ╔═╡ f0a23549-6365-48a9-aa7e-0e93adba2781
md"""
Вообще, это не гайд по блокнотам `Pluto.jl`, это прямая копипаста [оффициальной документации JuMP.jl](https://jump.dev/JuMP.jl/stable/tutorials/getting_started/getting_started_with_JuMP/). Прямо пример с их страницы. Дальше примеры можешь брать оттуда, разбираться самостоятельно. В общем, удачи.
"""

# ╔═╡ c8e0fd2e-93f5-4201-a1b6-9a9e5002b199
model = Model(HiGHS.Optimizer)

# ╔═╡ e5e7c177-3fee-461b-a4d5-be43558b816b
md"""
JuMP постепенно дополняет проблему в объекте модели. Создайте модель, передав оптимизатор в функцию `Model`:
"""

# ╔═╡ e50b7e5f-a917-4ab1-a64c-b89beb4e3146
@variable(model, x >= 0)

# ╔═╡ 596c9522-7ba1-46b0-8632-0bcb67f5ec83
md"""
Переменные моделируются с помощью `@variable`
"""

# ╔═╡ 79af215f-f30f-49ff-916d-f6b3bc5be523
@variable(model, 0 <= y <= 30)

# ╔═╡ 119c3a1d-94dd-4625-8819-120e9a6c128b
md"""
Переменные могут иметь нижнюю и верхнюю границы
"""

# ╔═╡ f835a4b7-3d6a-4cd7-b92e-7873ccc0615b
@objective(model, Min, 12x + 20y)

# ╔═╡ 912dc29f-5398-40e5-b4da-0d9e92813eca
md"""
Цель задается с помощью @objective
"""

# ╔═╡ 398d6e78-7f77-4148-b5d4-75699792fc76
@constraint(model, c1, 6x + 8y >= 100)

# ╔═╡ 17b5490a-b8e4-4772-8cfd-297631c3601b
@constraint(model, c2, 7x + 12y >= 120)

# ╔═╡ f3b0745f-0ad4-43ec-8a3d-111044878173
md"""
Ограничения моделируются с помощью `@constraint`. Здесь `c1` и `c2` — имена наших ограничений
"""

# ╔═╡ 325999ae-f1e2-4a81-9b23-c2eae2b8edb3
print(model)

# ╔═╡ 8f1b988a-234c-444d-be34-ac2bc653bf19
md"""
Хотя обычно в блокнотах `Pluto` не принято пользоваться консольным выводом, в гайде предложено вызвать `print`, чтобы отобразить модель
"""

# ╔═╡ 39b587de-3f9b-4df5-8a68-13c503942bc2
optimize!(model)

# ╔═╡ 23a210ae-90c9-476f-91d3-ff0d53ad9df5
md"""
Чтобы решить задачу оптимизации, вызовите команду optimize! с моделью в качестве аргумента
"""

# ╔═╡ 80410046-dfad-426c-bf5e-c761bdcbef82
md"""
### Дальше, собсна, идите по [гайду](https://jump.dev/JuMP.jl/stable/tutorials/getting_started/getting_started_with_JuMP/), разбирайте, тыр пыр
"""

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
HiGHS = "87dc4568-4c63-4d18-b0c0-bb2238e4078b"
JuMP = "4076af6c-e467-56ae-b986-b466b2749572"

[compat]
HiGHS = "~1.7.2"
JuMP = "~1.15.1"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.9.2"
manifest_format = "2.0"
project_hash = "7ad3bfc5737657e6c68918c6418f78b62afeff4c"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"
version = "1.1.1"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "d9a9701b899b30332bbcb3e1679c41cce81fb0e8"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.3.2"

[[deps.Bzip2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "19a35467a82e236ff51bc17a3a44b69ef35185a2"
uuid = "6e34b625-4abd-537c-b88f-471c36dfa7a0"
version = "1.0.8+0"

[[deps.CodecBzip2]]
deps = ["Bzip2_jll", "Libdl", "TranscodingStreams"]
git-tree-sha1 = "ad41de3795924f7a056243eb3e4161448f0523e6"
uuid = "523fee87-0ab8-5b00-afb7-3ecf72e48cfd"
version = "0.8.0"

[[deps.CodecZlib]]
deps = ["TranscodingStreams", "Zlib_jll"]
git-tree-sha1 = "02aa26a4cf76381be7f66e020a3eddeb27b0a092"
uuid = "944b1d66-785c-5afd-91f1-9de20f533193"
version = "0.7.2"

[[deps.CommonSubexpressions]]
deps = ["MacroTools", "Test"]
git-tree-sha1 = "7b8a93dba8af7e3b42fecabf646260105ac373f7"
uuid = "bbf7d656-a473-5ed7-a52c-81e309532950"
version = "0.3.0"

[[deps.Compat]]
deps = ["UUIDs"]
git-tree-sha1 = "8a62af3e248a8c4bad6b32cbbe663ae02275e32c"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.10.0"
weakdeps = ["Dates", "LinearAlgebra"]

    [deps.Compat.extensions]
    CompatLinearAlgebraExt = "LinearAlgebra"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "1.0.5+0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3dbd312d370723b6bb43ba9d02fc36abade4518d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.15"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DiffResults]]
deps = ["StaticArraysCore"]
git-tree-sha1 = "782dd5f4561f5d267313f23853baaaa4c52ea621"
uuid = "163ba53b-c6d8-5494-b064-1a9d43ac40c5"
version = "1.1.0"

[[deps.DiffRules]]
deps = ["IrrationalConstants", "LogExpFunctions", "NaNMath", "Random", "SpecialFunctions"]
git-tree-sha1 = "23163d55f885173722d1e4cf0f6110cdbaf7e272"
uuid = "b552c78f-8df3-52c6-915a-8e097449b14b"
version = "1.15.1"

[[deps.DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "2fb1e02f2b635d0845df5d7c167fec4dd739b00d"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.9.3"

[[deps.Downloads]]
deps = ["ArgTools", "FileWatching", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"
version = "1.6.0"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.ForwardDiff]]
deps = ["CommonSubexpressions", "DiffResults", "DiffRules", "LinearAlgebra", "LogExpFunctions", "NaNMath", "Preferences", "Printf", "Random", "SpecialFunctions"]
git-tree-sha1 = "cf0fe81336da9fb90944683b8c41984b08793dad"
uuid = "f6369f11-7733-5829-9624-2563aa707210"
version = "0.10.36"

    [deps.ForwardDiff.extensions]
    ForwardDiffStaticArraysExt = "StaticArrays"

    [deps.ForwardDiff.weakdeps]
    StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"

[[deps.HiGHS]]
deps = ["HiGHS_jll", "MathOptInterface", "PrecompileTools", "SparseArrays"]
git-tree-sha1 = "dc62a0bb7b13cab77b6992f0b778d3c79b5ea30c"
uuid = "87dc4568-4c63-4d18-b0c0-bb2238e4078b"
version = "1.7.2"

[[deps.HiGHS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl"]
git-tree-sha1 = "10bf0ecdf70f643bfc1948a6af0a98be3950a3fc"
uuid = "8fd58aa0-07eb-5a78-9b36-339c94fd15ea"
version = "1.6.0+0"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.IrrationalConstants]]
git-tree-sha1 = "630b497eafcc20001bba38a4651b327dcfc491d2"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.2.2"

[[deps.JLLWrappers]]
deps = ["Artifacts", "Preferences"]
git-tree-sha1 = "7e5d6779a1e09a36db2a7b6cff50942a0a7d0fca"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.5.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "31e996f0a15c7b280ba9f76636b3ff9e2ae58c9a"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.4"

[[deps.JuMP]]
deps = ["LinearAlgebra", "MacroTools", "MathOptInterface", "MutableArithmetics", "OrderedCollections", "Printf", "SnoopPrecompile", "SparseArrays"]
git-tree-sha1 = "3700a700bc80856fe673b355123ae4574f2d5dfe"
uuid = "4076af6c-e467-56ae-b986-b466b2749572"
version = "1.15.1"

    [deps.JuMP.extensions]
    JuMPDimensionalDataExt = "DimensionalData"

    [deps.JuMP.weakdeps]
    DimensionalData = "0703355e-b756-11e9-17c0-8b28908087d0"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"
version = "0.6.3"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"
version = "7.84.0+0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"
version = "1.10.2+0"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "OpenBLAS_jll", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.LogExpFunctions]]
deps = ["DocStringExtensions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "7d6dd4e9212aebaeed356de34ccf262a3cd415aa"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.26"

    [deps.LogExpFunctions.extensions]
    LogExpFunctionsChainRulesCoreExt = "ChainRulesCore"
    LogExpFunctionsChangesOfVariablesExt = "ChangesOfVariables"
    LogExpFunctionsInverseFunctionsExt = "InverseFunctions"

    [deps.LogExpFunctions.weakdeps]
    ChainRulesCore = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
    ChangesOfVariables = "9e997f8a-9a97-42d5-a9f1-ce6bfc15e2c0"
    InverseFunctions = "3587e190-3f89-42d0-90ee-14403ec27112"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "9ee1618cbf5240e6d4e0371d6f24065083f60c48"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.11"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MathOptInterface]]
deps = ["BenchmarkTools", "CodecBzip2", "CodecZlib", "DataStructures", "ForwardDiff", "JSON", "LinearAlgebra", "MutableArithmetics", "NaNMath", "OrderedCollections", "PrecompileTools", "Printf", "SparseArrays", "SpecialFunctions", "Test", "Unicode"]
git-tree-sha1 = "5c9f1e635e8d491297e596b56fec1c95eafb95a3"
uuid = "b8f27783-ece8-5eb3-8dc8-9495eed66fee"
version = "1.20.1"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"
version = "2.28.2+0"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"
version = "2022.10.11"

[[deps.MutableArithmetics]]
deps = ["LinearAlgebra", "SparseArrays", "Test"]
git-tree-sha1 = "6985021d02ab8c509c841bb8b2becd3145a7b490"
uuid = "d8a4904e-b15c-11e9-3269-09a3773c0cb0"
version = "1.3.3"

[[deps.NaNMath]]
deps = ["OpenLibm_jll"]
git-tree-sha1 = "0877504529a3e5c3343c6f8b4c0381e57e4387e4"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "1.0.2"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"
version = "1.2.0"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.21+4"

[[deps.OpenLibm_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"
version = "0.8.1+0"

[[deps.OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "13652491f6856acfd2db29360e1bbcd4565d04f1"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.5+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "2e73fe17cac3c62ad1aebe70d44c963c3cfdc3e3"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.6.2"

[[deps.Parsers]]
deps = ["Dates", "PrecompileTools", "UUIDs"]
git-tree-sha1 = "716e24b21538abc91f6205fd1d8363f39b442851"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.7.2"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "FileWatching", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
version = "1.9.2"

[[deps.PrecompileTools]]
deps = ["Preferences"]
git-tree-sha1 = "03b4c25b43cb84cee5c90aa9b5ea0a78fd848d2f"
uuid = "aea7be01-6a6a-4083-8856-8a6e6704d82a"
version = "1.2.0"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "00805cd429dcb4870060ff49ef443486c262e38e"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.4.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SnoopPrecompile]]
deps = ["Preferences"]
git-tree-sha1 = "e760a70afdcd461cf01a575947738d359234665c"
uuid = "66db9d55-30c0-4569-8b51-7e840670fc0c"
version = "1.0.3"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["Libdl", "LinearAlgebra", "Random", "Serialization", "SuiteSparse_jll"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SpecialFunctions]]
deps = ["IrrationalConstants", "LogExpFunctions", "OpenLibm_jll", "OpenSpecFun_jll"]
git-tree-sha1 = "e2cfc4012a19088254b3950b85c3c1d8882d864d"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "2.3.1"

    [deps.SpecialFunctions.extensions]
    SpecialFunctionsChainRulesCoreExt = "ChainRulesCore"

    [deps.SpecialFunctions.weakdeps]
    ChainRulesCore = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"

[[deps.StaticArraysCore]]
git-tree-sha1 = "36b3d696ce6366023a0ea192b4cd442268995a0d"
uuid = "1e83bf80-4336-4d27-bf5d-d5a4f845583c"
version = "1.4.2"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
version = "1.9.0"

[[deps.SuiteSparse_jll]]
deps = ["Artifacts", "Libdl", "Pkg", "libblastrampoline_jll"]
uuid = "bea87d4a-7f5b-5778-9afe-8cc45184846c"
version = "5.10.1+6"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"
version = "1.0.3"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"
version = "1.10.0"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.TranscodingStreams]]
deps = ["Random", "Test"]
git-tree-sha1 = "9a6ae7ed916312b41236fcef7e0af564ef934769"
uuid = "3bb67fe8-82b1-5028-8e26-92a6c54297fa"
version = "0.9.13"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"
version = "1.2.13+0"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.8.0+0"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"
version = "1.48.0+0"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
version = "17.4.0+0"
"""

# ╔═╡ Cell order:
# ╟─6825af41-9c57-49fb-b3f3-163340bb056b
# ╟─5e77a508-92d5-42a9-a38b-c1ed6f6ea820
# ╟─266919f1-a494-4372-92cd-36d1b41be0c9
# ╟─c7189e1f-5bb9-4a21-83f9-fcad9c61336b
# ╠═5c48657b-ba81-42f1-a342-84e6afa65844
# ╠═32040a18-50f2-4c36-a415-7c49a06ee507
# ╟─eccd5bb4-93fc-499a-b490-e5c23464c411
# ╠═03d91ab4-63ab-11ee-0d25-21124b615428
# ╟─1cdeca84-fec7-4b86-8be0-3aa742dc81e6
# ╟─f0a23549-6365-48a9-aa7e-0e93adba2781
# ╠═c8e0fd2e-93f5-4201-a1b6-9a9e5002b199
# ╟─e5e7c177-3fee-461b-a4d5-be43558b816b
# ╠═e50b7e5f-a917-4ab1-a64c-b89beb4e3146
# ╟─596c9522-7ba1-46b0-8632-0bcb67f5ec83
# ╠═79af215f-f30f-49ff-916d-f6b3bc5be523
# ╟─119c3a1d-94dd-4625-8819-120e9a6c128b
# ╠═f835a4b7-3d6a-4cd7-b92e-7873ccc0615b
# ╟─912dc29f-5398-40e5-b4da-0d9e92813eca
# ╠═398d6e78-7f77-4148-b5d4-75699792fc76
# ╠═17b5490a-b8e4-4772-8cfd-297631c3601b
# ╟─f3b0745f-0ad4-43ec-8a3d-111044878173
# ╠═325999ae-f1e2-4a81-9b23-c2eae2b8edb3
# ╟─8f1b988a-234c-444d-be34-ac2bc653bf19
# ╠═39b587de-3f9b-4df5-8a68-13c503942bc2
# ╟─23a210ae-90c9-476f-91d3-ff0d53ad9df5
# ╟─80410046-dfad-426c-bf5e-c761bdcbef82
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
